<?php

/**
 * @file
 * Drupal Module: Web Server Logging and Alerts.
 *
 * Sends logs and alerts to the web server's log.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Implements hook_help().
 */
function errorlog_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'errorlog.configuration':
      return '<p>' . t('Sends logs and alerts to the web server\'s error log.') . '</p>';
  }
}

/**
 * Implements hook_theme().
 */
function errorlog_theme() {
  return [
    'errorlog_format' => [
      'variables' => ['log' => NULL],
    ],
  ];
}

/**
 * Prepares variables for errorlog format templates.
 *
 * Default template: errorlog-format.html.twig.
 */
function template_preprocess_errorlog_format(&$variables) {
  global $base_root;
  $config = \Drupal::config('system.site');
  $log_data = $variables['log'];
  $log_context = $variables['log']['context'];
  $severity_list = RfcLogLevel::getLevels();

  // Filter out invalid arguments.
  // @see \Drupal\Component\Render\FormattableMarkup::placeholderFormat()
  $args = array_filter($log_context, function($key) {
    return in_array($key[0], ['@', ':', '%']);
  }, ARRAY_FILTER_USE_KEY);
  $message_body = new FormattableMarkup($log_data['message'], $args);

  $message = $config->get('name') ?: 'Drupal';
  $message .= '|' . $base_root;
  $message .= '|severity=' . $severity_list[$log_data['level']];
  $message .= '|type=' . $log_context['channel'];
  $message .= '|ip=' . $log_context['ip'];
  $message .= '|uri=' . $log_context['request_uri'];
  $message .= '|referer=' . $log_context['referer'];
  $message .= '|uid=' . $log_context['uid'];
  $message .= '|link=' . strip_tags($log_context['link']);
  $message .= '|message=' . strip_tags($message_body);
  $variables['message'] = $message;
}
